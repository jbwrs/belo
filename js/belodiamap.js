$(document).ready(function() {

        
    /* FADE IMAGES */
    $('.button, .facebook, .twitter, .mapimg, .workimg, .btnseework, .mapimg, .backthumbs, .moreinfo, .css_next, .css_prev, .css_prev_nav, .css_next_nav, .prev, .next, .seeproject').append('<span class="hover"></span>').each(function () {
        var $span = $('> span.hover', this).css('opacity', 0);
        $(this).hover(function () {
        $span.stop().fadeTo(500, 1);
        }, function () {
            $span.stop().fadeTo(500, 0);
        });
    });
        
    /* OVERLAY GOOGLE MAPS */
    $("a[rel^='prettyPhoto']").prettyPhoto({animationSpeed:'fast',theme:'dark_square',slideshow:5000});
    $("#map a[rel^='prettyPhoto']:first").prettyPhoto({
        custom_markup: '<div id="map_canvas" style="width:1000px; height:500px"></div>',
        changepicturecallback: function(){ initialize(); }
    });
    

    /*END MORE INFO PROJECTS*/
    

});