$(window).load(function() {
	$('#slider').nivoSlider({
		effect: 'fade', // Specify sets like: 'fold,fade,sliceDown'
		slices: 15, // For slice animations
		animSpeed: 1000, // Slide transition speed
		pauseTime: 4000, // How long each slide will show
		startSlide: 0, // Set starting Slide (0 index)
		directionNav: false, // Next & Prev navigation
		directionNavHide: true, // Only show on hover
		controlNav: false, // 1,2,3... navigation
		controlNavThumbs: false, // Use thumbnails for Control Nav
		controlNavThumbsFromRel: false, // Use image rel for thumbs
		keyboardNav: true, // Use left & right arrows
		pauseOnHover: true, // Stop animation while hovering
		manualAdvance: false, // Force manual transitions
		captionOpacity: false, // Universal caption opacity
		prevText: false, // Prev directionNav text
		nextText: false, // Next directionNav text		
	});
});