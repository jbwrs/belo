

function sendemail(){
		//Custom variables
		var sendTo = "yourdress@belodiabridal.com"; //"yourmail@yourdomain.com"; //send the form elements to this email
		var subject = "Message from website"; //subject of the email
		var nameErr = "Please enter a Name"; //Error message when Name field is empty
		var emailErr = "Please enter a valid Email"; //Error message when Email field is empty or email is not valid
		var messageErr = "Please enter a Message"; //Error message when Message field is empty

		//Reset field errors/variables
		$('#name_error').html("");
		$('#email_error').html("");
		$('#message_error').html("");
		var err = 0;

    // Check fields
    var name = $('#name').val();
    var email = $('#email').val();
    var emailVer = validate_email(email);
    var message =  $('#message').val();
	var message_str = "Message: ";
	var wedding_date = $('#wedding_date').val();
	var venue = $('#venue').val();
	var dress_budget = $('#dress_budget').val();
	var na = "N/A";

    if (!name || name.length == 0 || name == "Name")
    {
        $('#name_error').html(nameErr);
        err = 1;
    }
    if (!email || email.length == 0 || emailVer == 0)
    {
        $('#email_error').html(emailErr);
        err = 1;
    }
    if (!message || message.length == 0 || message == "How can we help you?")
    {
        $('#message_error').html(messageErr);
        err = 1;
    }
   	
   	//If there's no error submit form
		if(err == 0)
    {
		var str_form = "Name: " + name + "\r\nEmail: " + email + "\r\n"; 
		if (wedding_date != "Wedding Date" && wedding_date.length > 0) {
			str_form += "Wedding Date: " + wedding_date + "\r\n"; 
		}
		else { str_form += "Wedding Date: " + na + "\r\n"; }
		
		if (venue.length > 0 && venue != "Wedding Venue") {
			str_form += "Wedding Venue: " + venue + "\r\n"; 
		}
		else { str_form += "Wedding Venue: " + na + "\r\n"; }
		
		if (dress_budget.length > 0 && dress_budget != "Dress Budget") {
			str_form += "Dress Budget: " + dress_budget + "\r\n"; 
		}
		else { str_form += "Dress Budget: " + na + "\r\n"; }
		
		str_form += "Message: " + message; 
        // Request
        var data = {
            name: name,
            email: email,
            sendTo: sendTo,
            subject: subject,
            message: str_form  
        };
			
        // Send
        $.ajax({
            url: "http://www.belodiabridal.com/php/sendForm.php",
            dataType: "json",  
            type: "POST",
            data: data,
			beforeSend: function(xhrObj){
	            xhrObj.setRequestHeader("Accept","application/json");
	        },
			success: function(data){ 
     			$('#message_success').html("Email sent successfully!");
   			},
            error: function(XMLHttpRequest, textStatus, errorThrown)
            {
				console.log(textStatus.responseText);
            	console.log(errorThrown);
            	console.log(XMLHttpRequest);
                // Message
                $('#message_success').html("Error while contacting server, please try again.");
            }
        });
			
        // Message
        $('#message_success').html("Sending...");
    }

}

function validate_email(email) {
   var reg = /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i;
   if(reg.test(email) == false) {
      return 0;
   } else {
   		return 1;
   }
}
