/* page transition */
$(document).ready(function() {
	
	$(window).bind("unload", function() {
    });
	
	$("#index").css("display", "none");
    $("#index").fadeIn(400);
	$("#contentlayer").css("display", "none");
    $("#contentlayer").fadeIn(400);
	$("#box").css("display", "none");
    $("#box").fadeIn(400);
	$("#contentlayer_classic").css("display", "none");
    $("#contentlayer_classic").fadeIn(400);
	$("#contactcontent").css("display", "none");
    $("#contactcontent").fadeIn(400);
    
	$("a").click(function(event){
		event.preventDefault();
		linkLocation = this.href;
		$("body").fadeOut(350, redirectPage);		
	});
		
	function redirectPage() {
		window.location = linkLocation;
	}
	
});

