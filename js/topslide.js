$(document).ready(function() {
	var menuIndex = -1;
	
	// Expand Panel
	$("#open").click(function(){
		$('#menu ul li a').each(function(index){
			if($(this).hasClass("selected"))
				menuIndex = index;
			$(this).removeClass("selected");
		});
		
		$("#closec").addClass("selected");
	
		$("div#topslide").slideDown("slow");
	
	});	
	
	// Collapse Panel
	$("#close").click(function(){
		//alert(menuIndex);
		$("div#topslide").slideUp("slow");
		$('#menu ul li a').each(function(index){
			if(index == menuIndex)
				$(this).addClass("selected");
		});
		menuIndex = -1;
		$("#closec").removeClass("selected");	
	});		
	
	// Switch buttons from "open" to "close" on click
	$("#toggle a, #togglec a").click(function () {
		$("#toggle a, #togglec a").toggle();
	});
	
	// Expand Panel
	$("#openc").click(function(){
		
		$('#menu ul li a').each(function(index){
			if($(this).hasClass("selected"))
				menuIndex = index;
			$(this).removeClass("selected");
		});
		
		$("#closec").addClass("selected");
	
		$("div#topslide").slideDown("slow");
		
	});
	
	// Collapse Panel
	$("#closec").click(function(){
		$('#menu ul li a').each(function(index){
			if(index == menuIndex)
				$(this).addClass("selected");
		});
		menuIndex = -1;
		$("#closec").removeClass("selected");
		$("div#topslide").slideUp("slow");	
	});
	
});

$(document).ready(function() {
	
/* FADE LABEL INPUTS */
    $('.maila').dwFadingLinks({
        color: '#7E9046',
        duration: 700
    });
    $('.mail, .name, .email, .message, .wedding_date, .venue, .dress_budget').dwFadingLinks({
        color: '#fff',
        duration: 700
    });
    $('.text').dwFadingLinks({
        color: '#8f8f8f',
        duration: 700
    });
    
    /* FADE LINKS MENU */
    $('.lfade').dwFadingLinks({
        color: '#fff',
        duration: 400
    });
	
	/* FADE IMAGES */
    $('.button, .mapimg, .workimg, .btnseework, .mapimg').append('<span class="hover"></span>').each(function () {
        var $span = $('> span.hover', this).css('opacity', 0);
        $(this).hover(function () {
        $span.stop().fadeTo(500, 1);
        }, function () {
            $span.stop().fadeTo(500, 0);
        });
    });
	
	/* ASSIGN DEFAULT VALUES */
    $(document).ready(function() {
        $("#name").DefaultValue("Name");
        $("#email").DefaultValue("Email");
		$("#wedding_date").DefaultValue("Wedding Date");
        $("#venue").DefaultValue("Wedding Venue");
		$("#dress_budget").DefaultValue("Dress Budget");
        $("#message").DefaultValue("How can we help you?");
    });
	
});