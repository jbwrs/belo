<?php
header('Access-Control-Allow-Origin: *');

define( "RECIPIENT_NAME", "Belo Dia Bridal" );
define( "RECIPIENT_EMAIL", "yourdress@belodiabridal.com" );
define( "EMAIL_SUBJECT", "Website Message" );

// Read the form values
$success = false;
$senderName = isset( $_POST['name'] ) ? preg_replace( "/[^\.\-\' a-zA-Z0-9]/", "", $_POST['name'] ) : "";
$senderEmail = isset( $_POST['email'] ) ? preg_replace( "/[^\.\-\_\@a-zA-Z0-9]/", "", $_POST['email'] ) : "";
$message = isset( $_POST['message'] ) ? preg_replace( "/(From:|To:|BCC:|CC:|Subject:|Content-Type:)/", "", $_POST['message'] ) : "";

// If all values exist, send the email
if ( $senderName && $senderEmail && $message ) {
  $recipient = RECIPIENT_NAME . " <" . RECIPIENT_EMAIL . ">";
  $headers = "From: " . $senderName . " <" . $senderEmail . ">";
  $body = $message; //"Details:\n" . $message;
  $success = mail( $recipient, EMAIL_SUBJECT, $body, $headers );
}
else { echo $success; }

?>
